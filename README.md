## Retrofit
Retrofit is a type-safe HTTP client for OpenHarmony.

## How to Install

```javascript
ohpm install @ohos/retrofit
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).



### How to Use

#### Creating an HTTP Request
    Retrofit parses HTTP Request class and provides method based api calling. 
    As shown below, Create Custom HTTP request class like "DataService" Extending "BaseService".
    Retrofit supports API declaration with Decorators, for example to make "GET" api call use @GET. Other api's supported are listed under "API Introduction" below.
    Use "retrofit" module to import retrofit apis.

``` javascript
 // If the Path annotation has the same name as the system component Path, a project compilation failure may occur. Therefore, change Path to UrlPath during referencing.
import {BaseService,ServiceBuilder,GET,POST,DELETE,PUT,UrlPath,Body,BasePath,Response,Header,Query} from '@ohos/retrofit';

//BasePath - Append "/" to basepath.
@BasePath("/")
class DataService extends BaseService{
  
  // GET API call with Header and Query params
  @GET("get?test=arg123")
  async getRequest(@Header("Content-Type") contentType: string,@Query('d1') data1: string,@Query('d2') data2: number): Promise<Response<Data>> { return <Response<Data>>{} };

  // POST API call with Header and Body info
  @POST("post")
  async postRequest(@Header("Content-Type") contentType: string,@Body user: User): Promise<Response<Data>> { return <Response<Data>>{} };

  // PUT API call with Header and Body info
  @PUT("put")
  async putRequest(@Header("Content-Type") contentType: string,@Body user: User): Promise<Response<Data>> { return <Response<Data>>{} };

  // GET API call with Header, Query, and Path params
  @GET("{req}?test=arg123")
  async getRequest2(@Header("Content-Type") contentType: string,@Query('d1') data1: string,@Query('d2') data2: number,@Path("req") requestPath: string): Promise<Response<Data>> { return <Response<Data>>{} };
}
```

#### How to Initialize the Retrofit Service and Call Methods
    Initialize the Retrofit BaseService, Set End point and Call required apis by its method.

``` javascript
    const dataService = new ServiceBuilder()
      .setEndpoint("https://restapiservice.com")    // Base Url
      .setTransformResponse([(str :string)=>{       // allows response data to be changed before it is passed to then/catch.
        return JSON.parse(str,undefined,options)
      }])
      .build(DataService);

    dataService.getRequest("application/json","dat1",8).then((resp)=>{  // Get request with params
        console.log("getRequest Response =" + JSON.stringify(resp.result));
    }
```

### Available APIs

## BaseService.ServiceBuilder

| API     | Parameter                                       | Return Value        | Description                |
| ----------- | ------------------------------------------- | -------------- | -------------------- |
| setEndpoint | endpoint: string                            | ServiceBuilder | Sets **BaseURL**.         |
| setTimeout  | timeout: number                             | ServiceBuilder | Sets the request timeout duration.    |
| setTransformResponse | [(str :string)=>{}] | ServiceBuilder   | Sets **TransformResponse**|
| build<T>    | service: new (builder: ServiceBuilder) => T | T**            | Builds basic Retrofit services.|

  ## BaseService

| API     | Parameter            | Return Value| Description                     |
| ----------- | ---------------- | ------ |---------------------------------|
| setEndpoint | endpoint: string | void   | Sets **BaseURL**.               |
| clone       | N/A              | void   | Copies a basic service request. |



  ## Decorators

| API         | Parameter| Return Value| Description                                                        |
| --------------- | ---- | ------ | ------------------------------------------------------------ |
| @GET            | N/A  | void   | GET HTTP method-type decorator.                                         |
| @POST           | N/A  | void   | POST HTTP method-type decorator.                                         |
| @PUT            | N/A  | void   | PUT HTTP method-type decorator.                                         |
| @DELETE         | N/A  | void   | DELETE HTTP method-type decorator.                                       |
| @HEAD           | N/A  | void   | HEAD HTTP method-type decorator.                                        |
| @OPTIONS        | N/A  | void   | OPTIONS HTTP method-type decorator.                                     |
| @BasePath       | N/A  | void   | BasePath decorator used to append the base path.                            |
| @Path           | N/A  | void   | Path decorator (The **Path** annotation has the same name as the system component **Path**, which causes a project compilation failure. Therefore, use **Path as UrlPath** to rename **Path** as **UrlPath** in the entry file **index.ets**.)|
| @Body           | N/A  | void   | Body decorator used to parse a request body.                                  |
| @Headers        | N/A  | void   | Headers decorator used to set request headers.                                   |
| @Header         | N/A  | void   | Header decorator used to set a single request header.                                |
| @HeaderMap      | N/A  | void   | HeaderMap decorator used to set a header to a map object.                  |
| @Queries        | N/A  | void   | Decorator used to set a query list.                                   |
| @Query          | N/A  | void   | Decorator used to set a query.                                       |
| @QueryMap       | N/A  | void   | Decorator used to set a query in a map.                                |
| @FormUrlEncoded | N/A  | void   | FormUrlEncoded decorator used to enable **formurlencoding**.               |
| @Field          | N/A  | void   | Field decorator used to set **Field** for the **POST** method.                        |
| @FieldMap       | N/A  | void   | FieldMap decorator used to set **Field** using a map object.                    |
| @Timeout        | N/A  | void   | Timeout decorator used to set the request timeout duration.                             |



## Constraints

This project has been verified in the following versions:

DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)
DevEco Studio NEXT Developer Preview2: 4.1.3.600, OpenHarmony SDK: API 11 (5.0.0.19)
DevEco Studio NEXT Developer Beta2: 5.0.3.500, OpenHarmony SDK: API 12 (5.0.0.25)

## Directory Structure

```javascript
|---- retrofit  
|     |---- entry  # Sample code
|     |---- library  # retrofit library
|           |---- src # retrofit source code
|                 |---- # ets
|                         |---- # Service base class baseService.ts.
|                         |---- # constants.ts used to store constants such as the retrofit data type and interface.
|                         |---- # Basic data parser dataResolver.ts. JSON and XML parsers are supported.
|                         |---- # decorators.ts decorator used to encapsulate annotations.
|           |---- index.ts  # retrofit external APIs
|     |---- README.MD  # Readme                  
```

## How to Contribute

If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-tpc/retrofit/issues) or a [PR](https://gitee.com/openharmony-tpc/retrofit/pulls).

## License

This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-tpc/retrofit/blob/master/LICENSE).

/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Response } from '@ohos/retrofit';

export class HeadersData {
  "Content-Type"?: string;
  "Accept"?: string;
  "Allow"?: string;

  constructor(contentType?: string, accept?: string, allow?: string) {
    this["Content-Type"] = contentType;
    this["Accept"] = accept;
    this["Allow"] = allow;
  }
}

export class FieldsData {
  "key1": string;
  "key2": string;
  "key3": string;

  constructor(key1: string, key2: string, key3: string) {
    this["key1"] = key1;
    this["key2"] = key2;
    this["key3"] = key3;
  }
}

export class SizeData {
  "size": number;

  constructor(size: number) {
    this["size"] = size;
  }
}

export class ResultData {
  result: string;

  constructor(res: string) {
    this.result = res;
  }
}

export interface Data {
  args: ESObject,
  headers: ESObject,
  url: string
}

export interface EmployeeDetails {
  name: string;
  salary: number;
  age: number;
}

export interface DeleteQueryMapParams {
  id: number;
  hardDelete: boolean;
}

export class BlogDataType {
  title: string;
  content: string;
  readingVolume: number;

  constructor(title: string, content: string, readingVolume: number) {
    this.title = title;
    this.content = content;
    this.readingVolume = readingVolume;
  }
}

export function getResponseEmptyObject(): Response {
  return {} as Response;
}

export function getDataResponse(): Response<Data> {
  return {} as Response<Data>;
}

export function getESObjectResponse(): Response<ESObject> {
  return {} as Response<ESObject>;
}